#!/usr/bin/env python

""" tracker_camshift.py - Version 0.1 2010-12-28
    Date: June 06  2013
    Modification done by: Camilo P
    Which modifications: name of the topic
    Modification of the ROS OpenCV
"""

import math
import time
import thread
import sys
import rospy
import cv
import cv2
import numpy as np
import copy
from sensor_msgs.msg import Image
from visual_interface_kinova.msg import CamshiftBox
from visual_interface_kinova.msg import guiButtonState
from visual_interface_kinova.msg import centroidPixelLocation
#from wam_controller.srv import PlanGeneration        #REMOVE
from wam_controller_client.srv import GraspOrientation
#from std_srvs.srv import Empty  #REMOVE
from cv_bridge import CvBridge, CvBridgeError
from geometry_msgs.msg import Twist

BLACK = cv.CV_RGB(0, 0, 0)
GREEN = cv.CV_RGB(0, 255, 0)
RED = cv.CV_RGB(255, 0, 0)

def threaded_service_call(service, args):
    service.call(args)

class visualInterfaceKinova:
    def __init__(self):
        rospy.init_node('visualInterfaceKinova')

        self.ROI_camshift = rospy.Publisher("roi_camshift_planar_robot", CamshiftBox)
        self.guiState = rospy.Publisher("guiButtonState", guiButtonState)
        # Give the camera driver a moment to come up.
        rospy.sleep(1)

        # Create the window and make it re-sizeable (second parameter = 0)
        self.cv_window_name = "Kinova GUI"
        cv.NamedWindow(self.cv_window_name, 0)

        # Create the cv_bridge object
        self.bridge = CvBridge()

        # Subscribe to the raw camera image topic
        # /camera/rgb/image_color
        self.image_sub = rospy.Subscriber("/camera/rgb/image_rect_color", Image, self.callback)
        self.objectLocation_subscriber = rospy.Subscriber("centroidPixelLocation", centroidPixelLocation, self.handleObjectLocation, queue_size=1)
        self.cursor_subscriber = rospy.Subscriber("cursor", Twist, self.handleCursorMessage, queue_size=10)
        #REMOVE SERVICES
        self.wam_go_grasp_client=rospy.ServiceProxy('/wam_controller_client/wam_go_grasp',GraspOrientation)
        #       self.generate_plan_client = rospy.ServiceProxy('/arm_control/generate_plan', PlanGeneration)
        #       self.execute_plan_client = rospy.ServiceProxy('/arm_control/execute_plan', Empty)
        #self.hand_initialize = rospy.ServiceProxy('/arm_services/initialize_hand', Empty)
        #self.hand_open = rospy.ServiceProxy('/arm_services/open_grasp', Empty)
        #self.hand_close = rospy.ServiceProxy('/arm_services/close_grasp', Empty)
        #self.hand_spread_close = rospy.ServiceProxy('/arm_services/close_spread', Empty)
        #self.wam_go_home = rospy.ServiceProxy('/arm_services/go_home', Empty)
        #self.wam_initial_pos= rospy.ServiceProxy('/arm_services/go_initial_pos', Empty)

        #self.next_trajectory_point = rospy.ServiceProxy('/arm_control/next_trajectory_point', Empty)
        #self.prev_trajectory_point = rospy.ServiceProxy('/arm_control/prev_trajectory_point', Empty)

        self.generated_plan = 0

        cv.SetMouseCallback(self.cv_window_name, self.on_mouse)

        # Clicked cursor position
        self.cursor_x = 0
        self.cursor_y = 0

        self.xWorldCoordinate = 0
        self.yWorldCoordinate = 0
        self.zWorldCoordinate = 0
        #cursor State
        self.cursorState = 0

        #arrows configuration
        self.lCircleRGB = [0, 191, 255]
        self.rCircleRGB = [0, 191, 255]
        self.tCircleRGB = [0, 191, 255]

        #buttonConfiguration
        self.modeState = 0
        self.buttonXBoundaries = [530, 633]
        self.buttonYBoundaries = [20, 50]
        self.buttonColorRGB = [0, 191, 255]
        self.buttonCaptionLocation = [534, 42]
        self.buttonCaptionRGB = [255, 255, 255]

        #measuredConfiguration
        self.measureStateR = 0
        self.measureStateL = 0
        self.measurePointA = [0, 0, 0]
        self.measurePointB = [0, 0, 0]
        self.pixelPointA = [0, 0]
        self.pixelPointB = [0, 0]
        self.measureFlag = 0
        self.dist = 0.0
        self.measureLineRGB = [0, 0, 255]
        #simpleInterface
        self.simpleFlag = 1
        self.simpleObjectNumber = 0
        self.simpleObjectU = None
        self.simpleObjectV = None
        self.simpleIterator = 0
        self.simpleMouseMoveCounter = 0

        self.objectU = []
        self.objectV = []
        self.objectNumber = 0
        self.selectObjectU=0
        self.selectObjectV=0
        self.upOffSet = 50
        self.sideOffSet = 40
        self.lineLength = 40
        self.circleRadius = 10
        self.simpleActivateCursor=0
        self.mouseDisturbance=120
        self.simpleCursorState=0
        #self.pts=np.array([[10,5],[20,30],[70,20],[50,10]], np.int32)
        #self.pts=[[(40,50),(20,10)]]
        self.arrow_radius=40
        self.arrow_width=10
    def handleCursorMessage(self, data3):
        self.xWorldCoordinate = data3.linear.x
        self.yWorldCoordinate = data3.linear.y
        self.zWorldCoordinate = data3.linear.z

    def handleObjectLocation(self, data4):
        self.objectU = copy.copy(data4.u)
        self.objectV = copy.copy(data4.v)
        self.objectNumber = data4.numberOfObjects

    def callback(self, data):
        # Convert the raw image to OpenCV format using the convert_image() helper function
        cv_image = self.convert_image(data)
        self.do_camshift(cv_image)

        # Toggle between the normal and back projected image if user hits the 'b' key
        c = cv.WaitKey(7) % 0x100
        if c == 27:
            return
        elif c == ord("i"):
            try:
                #self.hand_initialize.call()
                print "initialize the hand"
            except:
                print "Unable to initialize the hand"
        elif c == ord("c"):
            try:
                #self.hand_close.call()
                print "close the hand"
            except:
                print "Unable to close the hand"
        elif c == ord("o"):
            try:
                #self.hand_open.call()
                print "open the hand"
            except:
                print "Unable to open the hand"
        elif c == ord("s"):
            try:
                #self.hand_spread_close.call()
                print "close hand spread"
            except:
                print "Unable to close hand spread"
        elif c == ord("q"):
            try:
                #self.wam_initial_pos.call()
                print "move wam"
            except:
                print "Unable to move wam"
        elif c == ord("h"):
            try:
                #self.wam_go_home.call()
                print "Unable to go home"
            except:
                print "Unable to go home"
        elif c == ord("n"):
            try:
		print "Interface next point call"
                #self.next_trajectory_point.call()
            except:
                print "Unable to go to next point"
        elif c == ord("p"):
            try:
		print "Interface prev point call"
                #self.prev_trajectory_point.call()
            except:
                print "Unable to go to prev point"


    def convert_image(self, ros_image):
        try:
            cv_image = cv.fromarray(self.bridge.imgmsg_to_cv2(ros_image, "bgr8"))
            return cv_image
        except CvBridgeError, e:
            print e


    def publish_cursor_info(self, cursor_x, cursor_y):
        roi_camshift = CamshiftBox()
        roi_camshift.x = cursor_x
        roi_camshift.y = cursor_y
        roi_camshift.a = 0
        roi_camshift.theta = 0
        roi_camshift.targetx = cursor_x
        roi_camshift.targety = cursor_y
        roi_camshift.savingFlag = 0
        self.ROI_camshift.publish(roi_camshift)

    def get_cursor_color(self):
        #Workspace Indicator
        if self.zWorldCoordinate == 0:
            targetCircleR = 105
            targetCircleB = 105
            targetCircleG = 105
        elif self.zWorldCoordinate < 1.5:  #this can be used to specify robot workspace
            targetCircleR = 255
            targetCircleB = 0
            targetCircleG = 0
        else:
            targetCircleR = 255
            targetCircleB = 255
            targetCircleG = 255

        return cv.CV_RGB(targetCircleR, targetCircleB, targetCircleG)

    def do_joystick_mode(self, cursor_x, cursor_y, cv_image):
        button_caption_color = cv.CV_RGB(self.buttonCaptionRGB[0], self.buttonCaptionRGB[1], self.buttonCaptionRGB[2])
        button_background_color = cv.CV_RGB(self.buttonColorRGB[0], self.buttonColorRGB[1], self.buttonColorRGB[2])
        cursor_color = self.get_cursor_color()

        if(self.measureStateR == 0):
            self.draw_mode_button(cv_image, " JOYSTICK")

            text_font = cv.InitFont(cv.CV_FONT_HERSHEY_SIMPLEX, 1, 1, 0, 1)
            printMessage = str(round(self.zWorldCoordinate, 2))+'m'
            cv.PutText(cv_image, printMessage, (cursor_x + 25, cursor_y), text_font, cv.CV_RGB(0, 255, 0))
        elif(self.measureStateR == 1):
            self.draw_mode_button(cv_image, " MEASURE")

            if(self.measureStateL == 0) and (self.measureFlag == 1):
                self.pixelPointA[0] = cursor_x
                self.pixelPointA[1] = cursor_y
                cv.Circle(cv_image, (self.pixelPointA[0], self.pixelPointA[1]), 5, cursor_color, 2)
                print "\t first point (x, y, z) before sleep %f, %f, %f" % (self.xWorldCoordinate, self.yWorldCoordinate, self.zWorldCoordinate)
                time.sleep(2)
                print "\t first point (x, y, z) after sleep %f, %f, %f" % (self.xWorldCoordinate, self.yWorldCoordinate, self.zWorldCoordinate)
                self.measurePointA[0] = self.xWorldCoordinate
                self.measurePointA[1] = self.yWorldCoordinate
                self.measurePointA[2] = self.zWorldCoordinate
                self.measureFlag = 0

            elif(self.measureStateL == 1) and (self.measureFlag == 1):
                self.pixelPointB[0] = cursor_x
                self.pixelPointB[1] = cursor_y
                print "\t second point (x, y, z) before sleep %f, %f, %f" % (self.xWorldCoordinate, self.yWorldCoordinate, self.zWorldCoordinate)
                time.sleep(2)
                print "\t second Point (x, y, z) after sleep %f, %f, %f" % (self.xWorldCoordinate, self.yWorldCoordinate, self.zWorldCoordinate)
                self.measurePointB[0] = self.xWorldCoordinate
                self.measurePointB[1] = self.yWorldCoordinate
                self.measurePointB[2] = self.zWorldCoordinate
                self.dist = math.sqrt(math.pow(self.measurePointA[0]-self.measurePointB[0], 2) + math.pow(self.measurePointA[1]-self.measurePointB[1], 2) + math.pow(self.measurePointA[2]-self.measurePointB[2], 2))

                self.measureFlag = 0

            if(self.measureStateL == 1):
                cv.Circle(cv_image, (self.pixelPointA[0], self.pixelPointA[1]), 5, cursor_color, 2)
                cv.Line(cv_image, (self.pixelPointA[0], self.pixelPointA[1]), (self.pixelPointB[0], self.pixelPointB[1]) , cv.CV_RGB(self.measureLineRGB[0], self.measureLineRGB[1], self.measureLineRGB[2]), 1, 8, 0)
                printMessage = str(round(self.dist, 3)*100)+'cm'
                text_font = cv.InitFont(cv.CV_FONT_HERSHEY_SIMPLEX, 1, 1, 0, 1)
                cv.PutText(cv_image, printMessage, (cursor_x+25, cursor_y), text_font, cv.CV_RGB(0, 255, 0))

    def do_camshift(self, cv_image):
        # Copy to a local variable to avoid a position switch while evaluating
        cursor_x = self.cursor_x
        cursor_y = self.cursor_y

        self.publish_cursor_info(cursor_x, cursor_y)

        gui_state = guiButtonState()
        gui_state.modeState = self.modeState
        gui_state.cursorState = self.cursorState
        self.guiState.publish(gui_state)

        cursor_color = self.get_cursor_color()
        if(self.modeState != 4):
            cv.Circle(cv_image, (cursor_x, cursor_y), 5, cursor_color, 2)

        button_caption_color = cv.CV_RGB(self.buttonCaptionRGB[0], self.buttonCaptionRGB[1], self.buttonCaptionRGB[2])
        button_background_color = cv.CV_RGB(self.buttonColorRGB[0], self.buttonColorRGB[1], self.buttonColorRGB[2])

        #JOYSTICK MODE
        if(self.modeState == 0):
            self.do_joystick_mode(cursor_x, cursor_y, cv_image)
        #AUTO-PICK MODE
        elif(self.modeState == 1):
            for n in range(0, int(self.objectNumber)):
                cv.Circle(cv_image, (int(self.objectU[n]), int(self.objectV[n])), 5, cv.CV_RGB(127, 255, 0), 2)
            self.draw_selection_target(cursor_x, cursor_y, cv_image)
            self.draw_mode_button(cv_image, "AUTO-PICK")
        #PICK MODE
        elif(self.modeState == 2):
            self.draw_mode_button(cv_image, "   PICK")
            self.draw_selection_target(cursor_x, cursor_y, cv_image)
        #PLACE MODE
        elif(self.modeState == 3):
            self.draw_mode_button(cv_image, "   PLACE")
            self.draw_selection_target(cursor_x, cursor_y, cv_image)
            #SIMPLE MODE
        elif(self.modeState == 4):

            if(self.simpleFlag == 1) and (self.simpleActivateCursor==0):
                self.simpleObjectNumber = self.objectNumber
                #self.objectU = copy.copy(data4.u)
                self.simpleObjectU = copy.copy(self.objectU)
                self.simpleObjectV = copy.copy(self.objectV)
                self.simpleFlag = 0
                self.simpleCursorState=2
            elif(self.simpleActivateCursor==1):
                #print "simpleMouseMoveCounter %f" % (self.simpleMouseMoveCounter)

                """
                if(self.simpleMouseMoveCounter<=(self.mouseDisturbance/3)):
                    #cv.Circle(cv_image, (self.selectObjectU - self.sideOffSet + self.circleRadius, self.selectObjectV), 10, cv.CV_RGB(255, 0, 0), -5)
                    self.lCircleRGB = [255, 0, 0]
                    self.tCircleRGB = [0, 0, 255]
                    self.rCircleRGB = [0, 0, 255]
                    self.simpleCursorState=2
                elif((self.mouseDisturbance/3)<self.simpleMouseMoveCounter<(self.mouseDisturbance*2/3)):
                    #cv.Circle(cv_image, (self.selectObjectU,self.selectObjectV - self.upOffSet + self.circleRadius), 10, cv.CV_RGB(255, 0, 0), -5)
                    self.lCircleRGB = [0, 0, 255]
                    self.tCircleRGB = [255, 0, 0]
                    self.rCircleRGB = [0, 0, 255]
                    self.simpleCursorState=1
                elif((self.mouseDisturbance*2/3)<=self.simpleMouseMoveCounter<self.mouseDisturbance):
                    #cv.Circle(cv_image, (self.selectObjectU + self.sideOffSet - self.circleRadius,self.selectObjectV), 10, cv.CV_RGB(255, 0, 0), -5)
                    self.lCircleRGB = [0, 0, 255]
                    self.tCircleRGB = [0, 0, 255]
                    self.rCircleRGB = [255, 0, 0]
                    self.simpleCursorState=3

                    """
                    #self.simpleActivateCursor=0
                self.draw_selection_target(self.selectObjectU,self.selectObjectV,cv_image)
                cv.Circle(cv_image, (self.selectObjectU,self.selectObjectV), 5, cv.CV_RGB(255, 0, 0), 2)
            elif(self.simpleActivateCursor==2):
                self.draw_selection_target(self.selectObjectU,self.selectObjectV,cv_image)
                cv.Circle(cv_image, (self.selectObjectU,self.selectObjectV), 5, cv.CV_RGB(255, 0, 0), 2)

                #self.simpleMouseMoveCounter
            else:
                for n in range(0, int(self.simpleObjectNumber)):
                    if(self.simpleIterator == n):
                        self.selectObjectU=int(self.simpleObjectU[n])
                        self.selectObjectV=int(self.simpleObjectV[n])
                        cv.Circle(cv_image, (self.selectObjectU,self.selectObjectV), 5, cv.CV_RGB(255, 0, 0), 2)
                    else:
                        cv.Circle(cv_image, (int(self.simpleObjectU[n]), int(self.simpleObjectV[n])), 5, cv.CV_RGB(127, 255, 0), 2)

            self.draw_mode_button(cv_image, "  SIMPLE")
        else:
            self.draw_mode_button(cv_image, "PULL/ROT")
            self.draw_PPR_target(cursor_x, cursor_y, cv_image)

        cv.ShowImage(self.cv_window_name, cv_image)

    def draw_mode_button(self, cv_image, mode):
        button_caption_color = cv.CV_RGB(self.buttonCaptionRGB[0], self.buttonCaptionRGB[1], self.buttonCaptionRGB[2])
        button_background_color = cv.CV_RGB(self.buttonColorRGB[0], self.buttonColorRGB[1], self.buttonColorRGB[2])
        cursor_color = self.get_cursor_color()
        cv.Rectangle(cv_image, (self.buttonXBoundaries[0], self.buttonYBoundaries[1]), (self.buttonXBoundaries[1], self.buttonYBoundaries[0]), button_background_color, cv.CV_FILLED)
        font = cv.InitFont(cv.CV_FONT_HERSHEY_COMPLEX, 0.5, 0.5, 0, 1, 8)
        cv.PutText(cv_image, mode, (self.buttonCaptionLocation[0], self.buttonCaptionLocation[1]), font, button_caption_color)

    def draw_selection_target(self, cursor_x, cursor_y, cv_image):
        # right ydown
        # Left arrow -->
        cv.Line(cv_image, (cursor_x - self.sideOffSet - self.lineLength / 2, cursor_y - self.lineLength / 2), (cursor_x - self.sideOffSet, cursor_y), BLACK, 3)
        cv.Line(cv_image, (cursor_x - self.sideOffSet - self.lineLength / 2, cursor_y + self.lineLength / 2), (cursor_x - self.sideOffSet, cursor_y), BLACK, 3)
        cv.Circle(cv_image, (cursor_x - self.sideOffSet + self.circleRadius, cursor_y), 10, cv.CV_RGB(self.lCircleRGB[0], self.lCircleRGB[1], self.lCircleRGB[2]), -5)

        # Right arrow
        cv.Line(cv_image, (cursor_x + self.sideOffSet + self.lineLength / 2, cursor_y - self.lineLength  /2), (cursor_x + self.sideOffSet, cursor_y), BLACK, 3)
        cv.Line(cv_image, (cursor_x + self.sideOffSet + self.lineLength / 2, cursor_y + self.lineLength  /2), (cursor_x + self.sideOffSet, cursor_y), BLACK, 3)
        cv.Circle(cv_image, (cursor_x + self.sideOffSet - self.circleRadius, cursor_y), 10, cv.CV_RGB(self.rCircleRGB[0], self.rCircleRGB[1], self.rCircleRGB[2]), -5)

        # Top arrow
        cv.Line(cv_image, (cursor_x, cursor_y-self.upOffSet), (cursor_x+self.lineLength/2, cursor_y-self.upOffSet-self.lineLength/2), BLACK, 3)
        cv.Line(cv_image, (cursor_x-self.lineLength/2, cursor_y-self.upOffSet-self.lineLength/2), (cursor_x, cursor_y-self.upOffSet), BLACK, 3)
        cv.Circle(cv_image, (cursor_x, cursor_y - self.upOffSet + self.circleRadius), 10, cv.CV_RGB(self.tCircleRGB[0], self.tCircleRGB[1], self.tCircleRGB[2]), -5)




# draw Pull Push rotate widgets

    def draw_PPR_target(self, cursor_x, cursor_y, cv_image):
        # right ydown
        # Left arrow -->

        #self.pts = self.pts.reshape((-1,1,2))
        #cv2.line(self.bridge.imgmsg_to_cv2(ros_image, "bgr8"),(15,20),(70,50),(255,0,0),5)
        #cv.PolyLine(img, polys, is_closed, color, thickness=1, lineType=8, shift=0)
        #self.pts=self.pts.reshape((-1,1,2))


        #initial_point
        points=[(cursor_x-self.arrow_radius,cursor_y)]
        
        divisions=20
        for i in range(1,divisions):
            x =(cursor_x-self.arrow_radius)+(self.arrow_radius/divisions)*i
            y =cursor_y-(self.arrow_radius**2-(x-cursor_x)**2)**0.5
            points=points+[(x,int(y))]
        #half_circle_point    
        points=points+[(cursor_x,cursor_y-self.arrow_radius)]
 
        for i in range(1,divisions):
            x =cursor_x+(self.arrow_radius/divisions)*i
            y =cursor_y-(self.arrow_radius**2-(x-cursor_x)**2)**0.5
            points=points+[(x,int(y))]

        #end_point
        points=points+[(cursor_x+self.arrow_radius,cursor_y)]
        #arrow
        points=points+[(cursor_x+self.arrow_radius-self.arrow_width,cursor_y),(cursor_x+self.arrow_radius+(self.arrow_width/2),cursor_y+self.arrow_width*2),(cursor_x+self.arrow_radius+self.arrow_width*2,cursor_y),(cursor_x+self.arrow_radius+self.arrow_width,cursor_y)]

        #outside arrow
        for i in range(divisions,1, -1):
            x =cursor_x+((self.arrow_radius+self.arrow_width)/divisions)*i
            y =cursor_y-((self.arrow_radius+self.arrow_width)**2-(x-cursor_x)**2)**0.5
            points=points+[(x,int(y))]
            
        #half_circle_point
        points=points+[(cursor_x,cursor_y-self.arrow_radius-self.arrow_width)]
 

        for i in range(divisions,1, -1):
            x =(cursor_x-(self.arrow_radius+self.arrow_width))+((self.arrow_radius+self.arrow_width)/divisions)*i
            y =cursor_y-((self.arrow_radius+self.arrow_width)**2-(x-cursor_x)**2)**0.5
            points=points+[(x,int(y))]
        #closing_point
        points=points+[(cursor_x-self.arrow_radius-self.arrow_width,cursor_y)]
       
        #arrow
        points=points+[(cursor_x-self.arrow_radius-self.arrow_width*2,cursor_y),(cursor_x-self.arrow_radius-self.arrow_width+(self.arrow_width/2),cursor_y+self.arrow_width*2),(cursor_x-self.arrow_radius+self.arrow_width,cursor_y)]


        #print(points)
        #print "center:(%d,%d)" % (cursor_x,cursor_y)
        #[[(cursor_x-arrow_radius,cursor_y),(cursor_x-(arrow_radius/4*3),10),(20,60)]]
        # [[(40,50),(20,10),(20,60)]]
        cv.PolyLine(cv_image, [points], True, GREEN,2)


        #points_pp=[(cursor_x-self.arrow_radius/2,cursor_y+self.arrow_radius/2)]
        
        #Left arrow
        # draw arrow tail
        arrow_magnitude=9
        arrow_length=self.arrow_width*3
        arrow_displacement=self.arrow_radius/2
        p=(cursor_x-arrow_displacement, cursor_y+arrow_displacement)
        q=(cursor_x-arrow_displacement, cursor_y+arrow_displacement+arrow_length)
        
        cv.Line(cv_image, p,q, RED, 2)
        # calc angle of the arrow
        angle = np.arctan2(p[1]-q[1], p[0]-q[0])
        # starting point of first line of arrow head
        p = (int(q[0] + arrow_magnitude * np.cos(angle + np.pi/4)),
             int(q[1] + arrow_magnitude * np.sin(angle + np.pi/4)))
        # draw first half of arrow head
        cv.Line(cv_image, p, q, RED, 2)
        # starting point of second line of arrow head
        p = (int(q[0] + arrow_magnitude * np.cos(angle - np.pi/4)),
             int(q[1] + arrow_magnitude * np.sin(angle - np.pi/4)))
        # draw second half of arrow head
        cv.Line(cv_image, p, q, RED, 2)


        #second_arrow
        p=(cursor_x+arrow_displacement, cursor_y+arrow_displacement+arrow_length)
        q=(cursor_x+arrow_displacement, cursor_y+arrow_displacement)
        cv.Line(cv_image, p,q, RED, 2)
        # calc angle of the arrow
        angle = np.arctan2(p[1]-q[1], p[0]-q[0])
        # starting point of first line of arrow head
        p = (int(q[0] + arrow_magnitude * np.cos(angle + np.pi/4)),
             int(q[1] + arrow_magnitude * np.sin(angle + np.pi/4)))
        # draw first half of arrow head
        cv.Line(cv_image, p, q, RED, 2)
        # starting point of second line of arrow head
        p = (int(q[0] + arrow_magnitude * np.cos(angle - np.pi/4)),
             int(q[1] + arrow_magnitude * np.sin(angle - np.pi/4)))
        # draw second half of arrow head
        cv.Line(cv_image, p, q, RED, 2)

        p=(cursor_x+arrow_displacement, cursor_y+arrow_displacement+arrow_length)
        q=(cursor_x+arrow_displacement, cursor_y+arrow_displacement)
 
        #push-pull circle
        cv.Circle(cv_image, (cursor_x, q[1]+(p[1]-q[1])/2), self.arrow_width, cv.CV_RGB(self.tCircleRGB[0], self.tCircleRGB[1], self.tCircleRGB[2]), -5)

        #rotation circle
        cv.Circle(cv_image, (cursor_x, cursor_y - self.arrow_radius-self.arrow_width/2), self.arrow_width, cv.CV_RGB(self.tCircleRGB[0], self.tCircleRGB[1], self.tCircleRGB[2]), -5)


        #cv.Line(cv_image, (cursor_x - self.sideOffSet - self.lineLength / 2, cursor_y - self.lineLength / 2), (cursor_x - self.sideOffSet, cursor_y), BLACK, 3)
        #cv.Line(cv_image, (cursor_x - self.sideOffSet - self.lineLength / 2, cursor_y + self.lineLength / 2), (cursor_x - self.sideOffSet, cursor_y), BLACK, 3)
        #cv.Circle(cv_image, (cursor_x - self.sideOffSet + self.circleRadius, cursor_y), 10, cv.CV_RGB(self.lCircleRGB[0], self.lCircleRGB[1], self.lCircleRGB[2]), -5)

        # Right arrow
        #cv.Line(cv_image, (cursor_x + self.sideOffSet + self.lineLength / 2, cursor_y - self.lineLength  /2), (cursor_x + self.sideOffSet, cursor_y), BLACK, 3)
        #cv.Line(cv_image, (cursor_x + self.sideOffSet + self.lineLength / 2, cursor_y + self.lineLength  /2), (cursor_x + self.sideOffSet, cursor_y), BLACK, 3)
        #cv.Circle(cv_image, (cursor_x + self.sideOffSet - self.circleRadius, cursor_y), 10, cv.CV_RGB(self.rCircleRGB[0], self.rCircleRGB[1], self.rCircleRGB[2]), -5)

        # Top arrow
        #cv.Line(cv_image, (cursor_x, cursor_y-self.upOffSet), (cursor_x+self.lineLength/2, cursor_y-self.upOffSet-self.lineLength/2), BLACK, 3)
        #cv.Line(cv_image, (cursor_x-self.lineLength/2, cursor_y-self.upOffSet-self.lineLength/2), (cursor_x, cursor_y-self.upOffSet), BLACK, 3)
      




    def in_mode_button(self, x, y):
        if(self.buttonXBoundaries[0] < x < self.buttonXBoundaries[1]) and (self.buttonYBoundaries[0] < y < self.buttonYBoundaries[1]):
            return True
        return False

    def is_circle_click(self, x, y):
        #TOP CIRCLE CLICK
        if(self.cursor_x-self.circleRadius < x < self.cursor_x+self.circleRadius) and (self.cursor_y-self.upOffSet < y < self.cursor_y-self.upOffSet+2*self.circleRadius) and (self.modeState != 0) and (self.modeState != 5):
            self.cursorState = 1
        #LEFT CIRCLE CLICK
        elif(self.cursor_x-self.sideOffSet < x < self.cursor_x-self.sideOffSet+2*self.circleRadius) and (self.cursor_y-self.circleRadius < y < self.cursor_y+self.circleRadius) and (self.modeState != 0) and (self.modeState != 5):
            self.cursorState = 2
        #RiGHT CIRCLE CLICK
        elif(self.cursor_x+self.sideOffSet-2*self.circleRadius < x < self.cursor_x+self.sideOffSet) and (self.cursor_y-self.circleRadius < y < self.cursor_y+self.circleRadius) and (self.modeState != 0) and (self.modeState != 5):
            self.cursorState = 3

        #TOP ROTATE CLICK
        elif(self.cursor_x-self.arrow_width < x < self.cursor_x+self.arrow_width) and (self.cursor_y - self.arrow_radius-self.arrow_width < y < self.cursor_y-self.arrow_radius+self.arrow_width/2) and (self.modeState != 0) and (self.modeState == 5):
            self.cursorState = 4

        #FRONT PULL/PUSH CLICK CP
        elif(self.cursor_x-self.arrow_width < x < self.cursor_x+self.arrow_width) and (self.cursor_y +self.arrow_radius/2+self.arrow_width*3/2-self.arrow_width/2 < y < self.cursor_y +self.arrow_radius/2+self.arrow_width*3/2+self.arrow_width/2) and (self.modeState != 0) and (self.modeState == 5):
            self.cursorState = 5

        else:
            return False
        return True

    def on_mouse(self, event, x, y, flags, param):
        if (event == cv.CV_EVENT_LBUTTONDOWN):
            print "\t mouse left button press"
            print "simpleCursorState:%f" % (self.simpleCursorState)
            print "simpleActivateCursor:%f" % (self.simpleActivateCursor)
            print "modeState:%f" % (self.modeState)

            if (self.modeState==4) and not(self.in_mode_button(x,y)) and (self.simpleActivateCursor==0):
                time.sleep(1)
                self.cursor_x = self.selectObjectU
                self.cursor_y = self.selectObjectV
                self.simpleActivateCursor=1
            elif(self.modeState==4) and not(self.in_mode_button(x,y)) and (self.simpleActivateCursor==1):

                self.reset_cursor_colors() #add by Oscar

                if(self.simpleCursorState==1):
                    print "generate_plan:%d" % (self.generated_plan)

                    try:
                        print "click TOP"
                        #self.generate_plan_client.call(1)
                        self.generated_plan = 1
                        self.tCircleRGB = [0, 255, 0]
                        self.simpleActivateCursor=2
                        self.cursorState = 1
                        #NEW: Send to top grasp
                        print "Por encima  antes"
                        thread.start_new_thread(threaded_service_call, (self.wam_go_grasp_client, 1))
                    except:
                        print "No plan generated"
                        self.generated_plan = -1
                        self.tCircleRGB = [255, 0, 0]
                        self.simpleActivateCursor=0

                    print "Por encima despues"

                elif(self.simpleCursorState==2):
                    print "generate_plan:%d" % (self.generated_plan)

                    try:
                        print "click LEFT"
                        #self.generate_plan_client.call(2)
                        self.generated_plan = 2
                        self.lCircleRGB = [0, 255, 0]
                        self.simpleActivateCursor=2
                        self.cursorState = 2
                        #NEW: Send to left grasp
                        thread.start_new_thread(threaded_service_call, (self.wam_go_grasp_client, 2))
                    except:
                        print "No plan generated"
                        self.generated_plan = -2
                        self.lCircleRGB = [255, 0, 0]
                        self.simpleActivateCursor=0

                elif(self.simpleCursorState==3):
                    print "generate_plan:%d" % (self.generated_plan)

                    try:
                        print "click RIGHT"
                        #self.generate_plan_client.call(3)
                        self.generated_plan = 3
                        self.rCircleRGB = [0, 255, 0]
                        self.simpleActivateCursor=2
                        self.cursorState = 3
                        #NEW: Send to Right grasp
                        thread.start_new_thread(threaded_service_call, (self.wam_go_grasp_client, 3))
                    except:
                        print "No plan generated"
                        self.generated_plan = -3
                        self.rCircleRGB = [255, 0, 0]
                        self.simpleActivateCursor=0

                print "simPle Activate Cursor 2"

            elif(self.modeState==4) and not(self.in_mode_button(x,y)) and (self.simpleActivateCursor==2):

                if self.generated_plan == self.simpleCursorState:
                    try:
                        #self.execute_plan_client.call()
                        #NEW: Cancel movement or finish it!!!!
                        self.reset_cursor_colors()
                    except:
                        print "Unable to execute plan"
                self.simpleActivateCursor=0
                self.simpleCursorState=0  #LAST
                print "simPle Activate Cursor 0"


            elif self.in_mode_button(x, y):
                self.buttonCaptionRGB = [0, 0, 0]
                self.modeState = (self.modeState+1) % 6 #CP was (self.modeState + 1)%6
                self.cursorState = 0



            elif self.is_circle_click(x, y):
                if self.generated_plan == self.cursorState or self.modeState==5:
                    try:
                        #self.execute_plan_client.call() #CHECK may be: self.wam_go_grasp_client.call(1)
                        print "self cursor state %f" % (self.cursorState)
                        thread.start_new_thread(threaded_service_call, (self.wam_go_grasp_client,
                                                                        self.cursorState))
                        self.reset_cursor_colors()
                    except:
                        print "Unable to execute plan"
            else:
                self.generated_plan = 0
                self.cursor_x = x
                self.cursor_y = y

        elif(event == cv.CV_EVENT_RBUTTONUP):
            if(self.measureStateR == 1):
                self.measureStateR = 0
            else:
                self.measureStateR = 1

        elif(event == cv.CV_EVENT_LBUTTONUP):
            self.buttonCaptionRGB = [255, 255, 255]
            self.measureFlag = 1
            if(self.measureStateL == 1):
                self.measureStateL = 0
            else:
                self.measureStateL = 1

        # SIMPLE_MODE
        elif(event == cv.CV_EVENT_MOUSEMOVE) and (self.modeState == 4):
            if(self.simpleIterator < self.simpleObjectNumber):
                if(self.simpleMouseMoveCounter==self.mouseDisturbance):
                    self.simpleIterator = self.simpleIterator+1
                    print "iterating"
                    print "simpleCursorState:%f" % (self.simpleCursorState)
                    print "simpleActivateCursor:%f" % (self.simpleActivateCursor)
                    print "modeState:%f" % (self.modeState)

                    self.simpleMouseMoveCounter=0
                else:
                    self.simpleMouseMoveCounter = self.simpleMouseMoveCounter+1
                    if(self.simpleActivateCursor==1):
                        if(self.simpleMouseMoveCounter<=(self.mouseDisturbance/3)):
                    #cv.Circle(cv_image, (self.selectObjectU - self.sideOffSet + self.circleRadius, self.selectObjectV), 10, cv.CV_RGB(255, 0, 0), -5)
                            self.lCircleRGB = [255, 0, 0]
                            self.tCircleRGB = [0, 0, 255]
                            self.rCircleRGB = [0, 0, 255]
                            self.simpleCursorState=2
                            self.cursorState=2
                        elif((self.mouseDisturbance/3)<self.simpleMouseMoveCounter<(self.mouseDisturbance*2/3)):
                    #cv.Circle(cv_image, (self.selectObjectU,self.selectObjectV - self.upOffSet + self.circleRadius), 10, cv.CV_RGB(255, 0, 0), -5)
                            self.lCircleRGB = [0, 0, 255]
                            self.tCircleRGB = [255, 0, 0]
                            self.rCircleRGB = [0, 0, 255]
                            self.simpleCursorState=1
                            self.cursorState=1
                        elif((self.mouseDisturbance*2/3)<=self.simpleMouseMoveCounter<self.mouseDisturbance):
                    #cv.Circle(cv_image, (self.selectObjectU + self.sideOffSet - self.circleRadius,self.selectObjectV), 10, cv.CV_RGB(255, 0, 0), -5)
                            self.lCircleRGB = [0, 0, 255]
                            self.tCircleRGB = [0, 0, 255]
                            self.rCircleRGB = [255, 0, 0]
                            self.simpleCursorState=3
                            self.cursorState=3
                    #self.simpleActivateCursor=0

            else:
                self.simpleFlag = 1
                self.simpleIterator = 0
        else:
            for i, circle_color in enumerate([self.tCircleRGB, self.lCircleRGB, self.rCircleRGB]):
                if (i+1) != self.cursorState and (i+1) != abs(self.generated_plan):
                    circle_color[0] = 0
                    circle_color[1] = 191
                    circle_color[2] = 255
            self.check_circle_mouse_over(x, y, event)

    def check_circle_mouse_over(self, x, y, event):
        # TOP CIRCLE
        if(event == cv.CV_EVENT_MOUSEMOVE) and (self.cursor_x-self.circleRadius < x < self.cursor_x+self.circleRadius) and (self.cursor_y-self.upOffSet < y < self.cursor_y-self.upOffSet+2*self.circleRadius) and (self.modeState != 0) and (self.modeState != 4):
            if abs(self.generated_plan) != 1:
                try:
                   # self.generate_plan_client.call(1) #CHECK may be replace by: self.wam_go_grasp_client.call(1)
                    self.generated_plan = 1
                    self.tCircleRGB = [0, 255, 0]
                except:
                    print "No plan generated"
                    self.generated_plan = -1
                    self.tCircleRGB = [255, 0, 0]
        # LEFT CIRCLE
        if(event == cv.CV_EVENT_MOUSEMOVE) and (self.cursor_x-self.sideOffSet < x < self.cursor_x-self.sideOffSet+2*self.circleRadius) and (self.cursor_y-self.circleRadius < y < self.cursor_y+self.circleRadius) and (self.modeState != 0) and (self.modeState != 4):
            if abs(self.generated_plan) != 2:
                try:
                    #self.generate_plan_client.call(2) #CHECK
                    self.generated_plan = 2
                    self.lCircleRGB = [0, 255, 0]
                except:
                    print "No plan generated"
                    self.generated_plan = -2
                    self.lCircleRGB = [255, 0, 0]
        # RIGHT CIRCLE
        if(event == cv.CV_EVENT_MOUSEMOVE) and (self.cursor_x+self.sideOffSet-2*self.circleRadius < x < self.cursor_x+self.sideOffSet) and (self.cursor_y-self.circleRadius < y < self.cursor_y+self.circleRadius) and (self.modeState != 0) and (self.modeState != 4):
            if abs(self.generated_plan) != 3:
                try:
                    #self.generate_plan_client.call(3) #CHECK
                    self.generated_plan = 3
                    self.rCircleRGB = [0, 255, 0]
                except:
                    print "No plan generated"
                    self.generated_plan = -3
                    self.rCircleRGB = [255, 0, 0]

    def reset_cursor_colors(self):
        self.lCircleRGB = [0, 191, 255]
        self.rCircleRGB = [0, 191, 255]
        self.tCircleRGB = [0, 191, 255]

def main(args):
    vn = visualInterfaceKinova()
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print "Shutting down visualInterfaceKinova"
    cv.DestroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)
